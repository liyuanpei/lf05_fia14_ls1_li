
public class ErsteLKlasse {

	public static void main(String[] args) {
		System.out.println("�bung 1:\n");
		System.out.println("Aufgabe 1:\n");
		System.out.print("Guten Morgen du \"graue\" Welt!\n");
		//Der Test der Ausgabe mit print und println
		System.out.println("Sch�ner Sonnenaufgang!\n");
		System.out.println("Aufgabe 2:\n");
		String s = "*************";
		System.out.printf("%9.1s\n", s);
		System.out.printf("%10.3s\n", s);
		System.out.printf("%11.5s\n", s);
		System.out.printf("%12.7s\n", s);
		System.out.printf("%13.9s\n", s);
		System.out.printf("%14.11s\n", s);
		System.out.printf("%15.13s\n", s);
		System.out.printf("%10.3s\n", s);
		System.out.printf("%10.3s\n", s);
		
		System.out.println("\n");
		
		
		double d1 = 22.4234234;
		double d2 = 111.2222; 
		double d3 = 4.0; 
		double d4 = 1000000.551; 
		double d5 = 97.34; 
		System.out.printf("%.2f\n", d1);
		System.out.printf("%.2f\n", d2);
		System.out.printf("%.2f\n", d3);
		System.out.printf("%.2f\n", d4);
		System.out.printf("%.2f\n\n", d5);
		
		System.out.println("�bung 2:\n");
		System.out.println("Aufgabe 1:\n");
		String s2 = "**";
		System.out.printf("%6s\n", s2);
		System.out.printf("%.1s %8.1s \n", s2, s2);
		System.out.printf("%.1s %8.1s \n", s2, s2);
		System.out.printf("%6s\n", s2);
		System.out.println("\n");
		System.out.println("Aufgabe 2:\n");
		String s3 = "=";
		System.out.printf("%s %5s %s %19s %4s \n", "0!", s3, "", s3, "1");
		System.out.printf("%s %5s %s %18s %4s \n", "1!", s3, "1", s3, "1");
		System.out.printf("%s %5s %s %14s %4s \n", "2!", s3, "1 * 2", s3, "2");
		System.out.printf("%s %5s %s %10s %4s \n", "3!", s3, "1 * 2 * 3", s3, "6");
		System.out.printf("%s %5s %s %6s %4s \n", "4!", s3, "1 * 2 * 3 * 4", s3, "24");
		System.out.printf("%s %5s %s %2s %4s \n", "5!", s3, "1 * 2 * 3 * 4 * 5", s3, "120");
		System.out.println("\n");
		
		System.out.println("Aufgabe 3:\n");
		
		System.out.printf("%-12s %s %10s \n", "Fahrenheit", "|", "Celsius");
		System.out.println("-------------------------");
		System.out.printf("%-12s %s %10.2f \n", "-20", "|", -28.8889);
		System.out.printf("%-12s %s %10.2f \n", "-10", "|", -23.3333);
		System.out.printf("%-12s %s %10.2f \n", "+0", "|", -17.7778);
		System.out.printf("%-12s %s %10.2f \n", "+20", "|", -6.6667);
		System.out.printf("%-12s %s %10.2f \n", "+30", "|", -1.1111);
		
		
		
	}

}
