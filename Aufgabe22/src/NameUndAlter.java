import java.util.Scanner;

public class NameUndAlter {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie ihren Namen ein: ");
		String name = myScanner.next();
		System.out.print("Bitte geben Sie ihr Alter ein: ");
		int alter = myScanner.nextInt();
		System.out.println("Hallo " + name + ". Dein Alter ist also " + alter + ". Nett dich kennenzulernen! :)");
		myScanner.close();
	}

}
