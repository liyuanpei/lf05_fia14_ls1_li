import java.util.Scanner;

class Fahrkartenautomat
{

    public static double fahrkatenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);

        double[] preisarray = new double[10];
        preisarray[0] = 2.9;
        preisarray[1] = 3.3;
        preisarray[2] = 3.6;
        preisarray[3] = 1.9;
        preisarray[4] = 8.6;
        preisarray[5] = 9.0;
        preisarray[6] = 9.6;
        preisarray[7] = 23.5;
        preisarray[8] = 24.3;
        preisarray[9] = 24.9;

        String[] bezeichnungarray = new String[10];
        bezeichnungarray[0] = "Einzelfahrschein Berlin AB";
        bezeichnungarray[1] = "Einzelfahrschein Berlin BC";
        bezeichnungarray[2] = "Einzelfahrschein Berlin ABC";
        bezeichnungarray[3] = "Kurzstrecke";
        bezeichnungarray[4] = "Tageskarte Berlin AB";
        bezeichnungarray[5] = "Tageskarte Berlin BC";
        bezeichnungarray[6] = "Tageskarte Berlin ABC";
        bezeichnungarray[7] = "Kleingruppen-Tageskarte Berlin AB";
        bezeichnungarray[8] = "Kleingruppen-Tageskarte Berlin BC";
        bezeichnungarray[9] = "Kleingruppen-Tageskarte Berlin ABC";

        double summe = 0.0;
        int ticketWahl = 1;
        double zuZahlenderBetrag;
        int anzahlTickets;
        while (ticketWahl != 0) {
            System.out.print("Wählen Sie ihre Wunschfahrkarte für aus:\n");

            for (int i = 0; i < bezeichnungarray.length; i++){
                System.out.printf("(%2d) %-35s [%.2f €] \n", i+1, bezeichnungarray[i], preisarray[i]);
            }

            System.out.print("Bezahlen (0)\n\n");
            while (true) {
                System.out.print("Ihre Wahl: ");
                ticketWahl = tastatur.nextInt();
                if (ticketWahl >= 0 && ticketWahl <= bezeichnungarray.length) {
                    break;
                }
                System.out.println(">>falsche Eingabe<<");
            }

            if (ticketWahl != 0) {

                while (true) {
                    System.out.print("Anzahl der Tickets: ");
                    anzahlTickets = tastatur.nextInt();
                    if (anzahlTickets >= 1 && anzahlTickets <= 10) {
                        break;
                    }
                    System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus. <<");
                }
                summe = summe + (preisarray[ticketWahl - 1] * anzahlTickets);
            }
        }

        return summe; //hier wird die anzahl an tickets mit dem Einzelticketpreis multipliziert, um auf den Endpreis zu kommen
    }

    public static double fahrkartenBezahlen(double zuZahlen) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlen - eingezahlterGesamtbetrag);
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    public static void muenzeAusgeben(double rueckgabeBetrag) {
        double[] muenzenBetrag = {2.0, 1.0, 0.5, 0.2, 0.1, 0.05};
        for (double muenze : muenzenBetrag) {
            while(rueckgabeBetrag >= muenze) {
                if(muenze >= 1) {
                    System.out.println((int)muenze + " EURO");
                } else {
                    System.out.println((int)(muenze*100) + " Cent");
                }
                rueckgabeBetrag -= muenze;
            }
            if(rueckgabeBetrag <= 0.0){
                break;
            }
        }
    }

    public static void rueckgeldAusgeben(double eingezahlterBetrag, double zuZahlenderBetrag) {
        double rueckgabebetrag = eingezahlterBetrag - zuZahlenderBetrag;
        if(rueckgabebetrag > 0.0)
        {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");
            muenzeAusgeben(rueckgabebetrag);
        }
    }

    public static void warte(int millisekunde) {
        try {
           Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;

        zuZahlenderBetrag = fahrkatenbestellungErfassen();

       // Geldeinwurf
       // -----------
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
        fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}