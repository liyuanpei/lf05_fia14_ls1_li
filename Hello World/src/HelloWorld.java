
public class HelloWorld {

	public static void main(String[] args) {
		int y = 7;
		int x;
		x = (y++); //1. x = y (7) 2. y++; -> x = 7 y = 8
		System.out.println("x = y++");
		System.out.println("x = " + x);
		System.out.println("y = " + y);
		y = 7;
		x = y = y + 1; // y = y + 1 -> 8 x = y -> x = 8 und y = 8
		System.out.println("x = y = y + 1");
		System.out.println("x = " + x);
		System.out.println("y = " + y);
		y = 7;
		y++;
		System.out.println(y);
	}

}
